<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Usuarios;
use App\Form\UsuariosType;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class DefaultController extends AbstractController
{

    public function index()
    {

        $usuarios = $this->getDoctrine()
		    ->getRepository(Usuarios::class)
		    ->findAll();

        return $this->render('default/index.html.twig', [
            'usuarios' => $usuarios
        ]);

    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit($id=0, Request $request, ValidatorInterface $validator)
    {

        $em = $this->getDoctrine()->getManager();
        
        $usuario = $this->getDoctrine()
                    ->getRepository(Usuarios::class)
                    ->find(intval($id));
        if (!$usuario){
            $usuario = new Usuarios();
        }
        
        $form = $this->createForm(UsuariosType::class, $usuario);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($usuario);
            $em->flush();
            $this->addFlash('success', $id==0 ? "Registro de Usuario Creado" : "Registro de Usuario Actualizado");
            return $this->redirectToRoute('index');
        }

        $response = $this->render('default/form.html.twig', [
            'form' => $form->createView(),
            'id'   => $id
        ]);

        return $response;

    }
  
}
