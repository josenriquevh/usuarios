<?php

namespace App\Form;

use App\Entity\Usuarios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuariosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            $dato = $event->getData();
            $form = $event->getForm();

            $readonly = false;
            if ($dato->getId()) {
                $readonly = true;
            }

            $form
                ->add('nombres', TextType::class, [
                    'label' => 'Nombres',
                    'attr' => [
                        'placeholder' => 'Primero y segundo nombre del usuario',
                    ]
                ])
                ->add('apellidos', TextType::class, [
                    'label' => 'Apellidos',
                    'attr' => [
                        'placeholder' => 'Primero y segundo apellido del usuario',
                    ]
                ])
                ->add('cedula', IntegerType::class, [
                    'label' => 'Cédula',
                    'attr' => [
                        'placeholder' => 'Cédula del usuario',
                    ]
                ])
                ->add('correo', EmailType::class, [
                    'label' => 'Correo',
                    'attr' => [
                        'placeholder' => 'Correo del usuario',
                        'readonly' => $readonly,
                    ]
                ])
                ->add('telefono', TextType::class, [
                    'label' => 'Teléfono',
                    'attr' => [
                        'placeholder' => 'Teléfono del usuario',
                    ],
                    'required' => false
                ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuarios::class,
        ]);
    }
}
