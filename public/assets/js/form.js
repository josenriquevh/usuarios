// /var/www/html/usuarios/public/assets/js/form.js

$(document).ready(function() {

    $('#form').validate({
        rules: {
            'usuarios[nombres]': {
                required: true
            },
            'usuarios[apellidos]': {
                required: true
            },
            'usuarios[cedula]': {
                required: true,
                digits: true
            },
            'usuarios[correo]': {
                required: true,
                email: true
            },
            'usuarios[telefono]': {
                digits: true
            }
        },
        messages: {
            'usuarios[nombres]': {
                required: "El campo de nombres no debe estar vacío."
            },
            'usuarios[apellidos]': {
                required: "El campo de apellidos no debe estar vacío."
            },
            'usuarios[cedula]': {
                required: "El campo de cédula no debe estar vacío.",
                digits: "La cédula debe ser numérica"
            },
            'usuarios[correo]': {
                required: "El campo de correo no debe estar vacío.",
                email: "El correo es inválido"
            },
            'usuarios[telefono]': {
                digits: "El teléfono debe ser numérico"
            }
        }
    });

    $('#guardar').click(function(){
        var valid = $("#form").valid();
        if (valid){
            $('#form').submit();
        }
    });
    
});

