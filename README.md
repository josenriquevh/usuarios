# Summary

Desarrollo de un listado de usuarios registrados con un formulario para la creació o modificación del registro de cada usuario.

## Referencias

[Proyecto desarrollado en Symfony 5.3.9](https://symfony.com/doc/current/index.html)

- [Installing & Setting up the Symfony Framework](https://symfony.com/doc/current/setup.html)
- [Creating the Project (Best Practices)](https://symfony.com/doc/current/best_practices.html)
- [Databases and the Doctrine ORM](https://symfony.com/doc/current/doctrine.html)
- [Routing](https://symfony.com/doc/current/routing.html)
- [Validation](https://symfony.com/doc/current/validation.html)


## Configuración del ambiente local

### Instalación

1. Clonar el proyecto  `git clone git@gitlab.com:josenriquevh/usuarios.git`.
2. Ejecutar `composer install`.
3. [Crear virtualhost](https://www.lunium.com/blog/crear-virtual-host-desde-ubuntu/). **Opcionalmente se puede ejecutar `php bin/console server:run` para levantar un servidor**.


        <VirtualHost *:80>
            ServerName     usuarios.net
            ServerAlias    usuarios.net

            DocumentRoot "${INSTALL_DIR}/www/usuarios/public"
            DirectoryIndex  index.php
            <Directory "${INSTALL_DIR}/www/usuarios/public">
                AllowOverride None
                Allow from All
                <IfModule mod_rewrite.c>
                    Options -MultiViews
                    RewriteEngine On
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteRule ^(.*)$ index.php [QSA,L]
                </IfModule>
            </Directory>

            SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
            SetEnv APP_ENV dev
        </VirtualHost>


4. Los parámetros de la BD se crean en el archivo _.env_. Aunque se puede crear el archivo _.env.local_ para colocar los parámetros propios de su ambiente local:
    `DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7`
5. Si es primera vez que se configura la BD, ejecutar el comando `php bin/console doctrine:database:create`. Esto creará la BD.
6. Ejecutar el siguiente comando para actualizar los atributos de la tabla de la BD: `php bin/console d:s:u --force`.
7. En la ruta _${INSTALL_DIR}/www/usuarios/_
    - Crear las carpetas cache y log (si no existen) con `mkdir -p var/cache var/log`.
    - Configurar permisos a las carpetas cache y log con `chmod -R 777 var/cache/ var/log/`.
    - Borrar el contenido de las carpetas cache y log con `rm -Rf var/cache/**/* var/log/*`.
9. Ejecutar los comandos para que se pueda eliminar la cache usando el comando de symfony. [Documentación](https://symfony.com/doc/current/cache.html#clearing-the-cache).

10. Si todo esta bien, ejecute `usuarios.net` en el navegador.


## Información adicional

- El desarrollo de la aplicación se realizó bajo un entorno en Ubuntu 20.04 con Apache.
- Versión de PHP 7.3.
- Composer 1.10.
- MySQL 5.7.
- IDE Visual Studio Code 1.57.
- Tiempo de desarrollo 10 horas-hombre.
- [Video explicativo de la funcionalidad](https://youtu.be/f5Av3onM54E).
